package exercicio.tdd;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	FizzBuzz a = new FizzBuzz();
    	assertEquals("FizzBuzz  14  13 Fizz  11 Buzz Fizz  8  7 Fizz Buzz  4 Fizz  2  1 ", a.validaSeqNumeros(15));	
    	
    }
}
