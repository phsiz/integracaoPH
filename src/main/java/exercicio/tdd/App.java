package exercicio.tdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    	
    	FizzBuzz a = new FizzBuzz();
    	System.out.println(a.validaSeqNumeros(15));	
    }
}
